<?php
session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>File Upload</title>
		<link rel="stylesheet" type="text/css" href="theme.css">
	</head>
	<body>
		<h1>Your files</h1>
		<?php
			if(session_id() == '' || !isset($_SESSION) || !isset($_SESSION['username'])) {
				header("Location: FileShare.html");
			}

			//------------------------------------------------------------
			//	GET SESSION VARIABLES AND FILENAMES FROM DIR
			//------------------------------------------------------------

			$username = $_SESSION['username']; //pull out username from array
			$full_path = "/home/jennyliu/private/MOD2/FilesUploaded/".$username; //path to put files
			$files = scandir($full_path);

			//------------------------------------------------------------
			//	OUTPUT FILE LINKS TO PAGE
			//------------------------------------------------------------

			for($i=0; $i<count($files); $i++){
				if( $files[$i] !== "." && $files[$i] !== ".." ) {
					$safe_file = htmlspecialchars($files[$i]);
					printf("<a href=getfile.php?filename=%s>%s</a>&nbsp;&nbsp;&nbsp;&nbsp;", $safe_file, $safe_file);
					printf("<a class=\"button\" href=deletefile.php?filename=%s>delete</a><br/><br/>", $safe_file, $safe_file);
				}
			}
		?>
		<h1>Upload a file</h1>
		<form enctype="multipart/form-data" action="uploader.php" method="POST">
			<p>
			<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
			<input name="uploadedfile" type="file" id="uploadfile_input" />
			</p>
			<p>
			<input type="submit" value="Upload File" />
			</p>
		</form>

		<h1>Logout</h1>
		<a class="button" href="logout.php">logout</a>
	</body>
</html>

