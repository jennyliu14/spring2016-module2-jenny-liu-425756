<?php
session_start();

//get the file name
$file = trim(htmlentities($_GET['fileName']));

//check if file is null
if($file == NULL){
    echo "Enter a non-null input";
    exit;
}

//variable for the path to the file
$full_path = '/home/jennyliu/private/MOD2/FilesUploaded'.$_SESSION['username'].'/'.$_GET['fileName'];

//if the file path is not valid, break
if(!file_exists($full_path)){
        echo "That file does not exist";
        exit;
}

//if the file path is valid, open it
$finfo = new finfo(FILEINFO_MIME_TYPE);
$mime = $finfo->file($full_path);
header("Content-Type: ".$mime);
readfile($full_path);

?>