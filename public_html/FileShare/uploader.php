<?php
session_start();

//------------------------------------------------------------
//	FILTERING INPUT
//------------------------------------------------------------

// Get the filename and make sure it is valid
$filename = basename($_FILES['uploadedfile']['name']);
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "No File Uploaded";
	exit;
}

// make sure the filename is valid
$filename = str_replace('/', '_', $filename);
$filename = str_replace('..', '', $filename);
 
// Get the username and make sure it is valid, keeps you logged in (checks if you are still the user stored in this session)
$username = $_SESSION['username'];
if( !preg_match('/^[\w_\-]+$/', $username) ){
	echo "Invalid username";
	exit;
}

//------------------------------------------------------------
//	SAVING FILE TO SERVER
//------------------------------------------------------------

//saves and stores file into full_path 
$full_path = "/home/jennyliu/private/MOD2/FilesUploaded/".$username;
 
if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path . "/" . $filename ) ){
	header("Location: fileupload.php");
	exit;
}else{
	header("Location: fileupload.php");
	exit;
}

?>
